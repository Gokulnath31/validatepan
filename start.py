from flask import Flask,jsonify,request
import requests
from bs4 import BeautifulSoup
import pytesseract
import shutil
import os
import random
import requests
import cv2
import io
import numpy as np
import re
import datetime


app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'VERIFYING PAN'

@app.route('/native_pan_verify',methods=["POST"])
def process():

    data=request.get_json()
    name=data["full_name"]
    pan_no=data["pan_number"]
    dob=data["dob"]
    status=data["type"]
    transcation_id=data["txn_id"]
    invalidity=invalid_input(pan_no,name,dob,status)
    if(invalidity):
        return jsonify({
            "status": "failed",
            "status_code": "502", 
            "message":"Invalid Input",
            "txn_id": transcation_id })
    result=validate(pan_no,name,dob,status,transcation_id)
    if(result=="508"):
        return jsonify({
            "status": "failed",
            "status_code": "508",
            "message": "Source website down",
            "txn_id": transcation_id})

    elif(result=="504"):
        return jsonify({"status": "failed",
                "status_code": "504",
                "message": "Issue in retrieving data from Source site",
                "txn_id": transcation_id
                })
    
    elif(result=="505"):
        return jsonify({
        "status": "failed",
        "status_code": "505",
        "message": "Unknown issue, try again after some time",
        "txn_id": transcation_id
        })

    elif("inactive" in result.lower() or "not active" in result.lower()):
        {
	    "status": "success",
	    "status_code": "200",
	    "result": {
        "pan_number": pan_no, 
        "status1": "PAN is not active"},
        "txn_id": transcation_id
        }


    elif("not matching" in result.lower()):
        return jsonify({"status": "success",
	    "status_code": "200",
	    "result": {
        "pan_number": pan_no, 
        "status1": "PAN is active", 
        "status2": "Some details are not matching with PAN database"},
        "txn_id": transcation_id 
        })
    


    elif("no record found" in result.lower()):
        return jsonify({
        "status": "failed",
        "status_code": "507",
        "message": "Record not found in PAN Database",
        "txn_id": transcation_id})
    
    return jsonify({"status": "success",
	    "status_code": "200",
	    "result": {
        "pan_number": pan_no, 
        "status1": "PAN is active",
        "status2": "All details are matching with PAN database"},
        "txn_id ":transcation_id
        })


def invalid_input(pan_no,name,dob,status):

    if((len(pan_no)!=10) or (status not in ["P","H","A","B","C","G","J","L","F","T"]) or (not(check_date(dob))) or(not(pan_no.isalnum()))):
        print(len(pan_no)!=10,status not in ["P","H","A","B","C","G","J","L","F","T"],not(check_date(dob)))
        return True
    return False

def check_date(dob):
    try:
        datetime.datetime.strptime(dob, '%d/%m/%Y')
        return True
    except Exception:
        return False


    
def validate(pan_no,name,dob,status,transcation_id):
    header = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36'}
    iterations=0
    while(iterations<5):
        iterations +=1
        session = requests.Session()

        url = "https://www1.incometaxindiaefiling.gov.in/e-FilingGS/Services/VerifyYourPanDeatils.html"
        
        try:
            response = session.get(url, headers=header, verify=False,timeout=15)

        except Exception:
            return "504"

        if(response.status_code==503):
            return "508"


        soup = BeautifulSoup(response.content, "html5lib")
        request_id = soup.find("input", attrs={"id": "VerifyYourPanGSAuthentication_requestId"}).get("value")
        img_src = soup.find("img", attrs={"class": "captchaImgBox"}).get("src")
        src = "https://www1.incometaxindiaefiling.gov.in"+img_src

        print(request_id, src)

        img_data = session.get(src, verify=False).content
        f_name = transcation_id+".jpg"
        with open(f_name, 'wb') as handler:
            handler.write(img_data)
        img = cv2.imread(f_name, cv2.IMREAD_UNCHANGED)
        
        kernele = np.ones((1, 3), np.uint8)
        erosion = cv2.erode(img, kernele, iterations=1)
        

        kerneld = np.ones((1, 1), np.uint8)
        dilation = cv2.dilate(erosion, kerneld, iterations=1)
        
        dilcaptcha = pytesseract.image_to_string(dilation, lang='eng', config='--psm 10 --oem 3 -c tessedit_char_whitelist=0123456789ABCDEFIJKLMNOPQRSTUVWXYZ -c load_system_dawg=false -c load_freq_dawg=false')

        print(dilcaptcha)
        captcha = "".join(re.findall("[0-9A-Z]", dilcaptcha))
        print(captcha)
        os.remove(f_name)
        if(len(captcha)!=6):
            continue

        data = {"requestId": request_id,
                "pan": pan_no,
                "fullName": "PRABAKARAN V",
                "dateOfBirth": dob,
                "status": status,
                "captchaCode": captcha}

        try:
            res = session.post("https://www1.incometaxindiaefiling.gov.in/e-FilingGS/Services/VerifyYourPanGSAuthentication.html",data=data, verify=False, headers=header,timeout=15)
        except Exception:
            return "504"

        ssoup = BeautifulSoup(res.content, "html.parser")
        temp=ssoup.find("div",attrs={"class":"success"})

        if(temp is not None):
            result=temp.li.text
            print(result)
            return result

    if(iterations==5 and(temp is None)):
        print("Something is wrong.Please check the details and try again.")
        return "505"

if __name__ == "__main__":
    app.run(debug=True)